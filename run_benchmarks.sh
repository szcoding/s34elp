#!/bin/bash

TIMEFORMAT='user:%U,system:%S'

DIR_BENCHM="./benchmarks"
DIR_EXP="./examples"

mkdir -p -- $DIR_BENCHM

for filename in ./examples/*.ehex; do
	fname=$(basename "$filename")
	base_name=${fname%.*}
	touch $DIR_BENCHM/$base_name"_ehex"
	touch $DIR_BENCHM/$base_name"_s34elp"
	touch $DIR_BENCHM/$base_name"_overall"	

    echo "file $base_name"

	for i in $(seq 1 2)
	do
	    echo "$i. run.."

		ehextime="$(time ( ehex "$filename" ) 2>&1 1>/dev/null )"
		echo "$ehextime" >> "$DIR_BENCHM/$base_name""_ehex"
		echo "EHEX,""$ehextime" >> "$DIR_BENCHM/$base_name""_overall"

		s34elptime="$(time ( java -jar ./target/s34elp.jar -r ./benchmarks/rtstat_"$base_name"_none.csv -f "$filename" ) 2>&1 1>/dev/null )"
		echo "$s34elptime" >> "$DIR_BENCHM/$base_name""_s34elp"
		echo "none,""$s34elptime" >> "$DIR_BENCHM/$base_name""_overall"

		s34elptime="$(time ( java -jar ./target/s34elp.jar -r ./benchmarks/rtstat_"$base_name"_HMODCOMPONLY.csv -f "$filename" -HMODCOMPONLY) 2>&1 1>/dev/null )"
		echo "$s34elptime" >> "$DIR_BENCHM/$base_name""_HMODCOMPONLY_s34elp"
		echo "HMODCOMPONLY,""$s34elptime" >> "$DIR_BENCHM/$base_name""_overall"

		s34elptime="$(time ( java -jar ./target/s34elp.jar -f "$filename" -HSINGLECOMP) 2>&1 1>/dev/null )"
		echo "$s34elptime" >> "$DIR_BENCHM/$base_name""_HSINGLECOMP_s34elp"
		echo "HSINGLECOMP,""$s34elptime" >> "$DIR_BENCHM/$base_name""_overall"

	done

done

TIMEFORMAT='\nreal\t%3lR\nuser\t%3lU\nsys\t%3lS'

./clean.sh
