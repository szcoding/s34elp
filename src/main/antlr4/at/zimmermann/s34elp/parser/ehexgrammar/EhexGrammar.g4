grammar EhexGrammar;

start:
    program EOF;


program:
    statements? query?
    ;

statements:
    (statement)* ;

statement:
    constraint # cons
    | r_rule # r_rul
    | weak_constraint # weak_con
    | optimize # opt;

constraint:
    CONS body? DOT
    ;

r_rule:
    head (CONS (body)?)? DOT
    ;

weak_constraint:
    WCONS body? DOT SQUARE_OPEN weight_at_level SQUARE_CLOSE
    ;

head:
    disjunction
    | choice ;

body:
	(extended_literal | aggregate_literal) (COMMA (extended_literal | aggregate_literal))*
    ;

disjunction:
	classical_literal (OR classical_literal)*
    ;

choice:
    term relational_op choice_set
        (relational_op term)?
    | choice_set (relational_op term)?
    ;

choice_set: CURLY_OPEN choice_elements CURLY_CLOSE;

choice_elements:
	choice_element (SEMICOLON choice_elements)*;

choice_element:
    classical_literal (COLON (extended_literals)?)?
    ;

aggregate_literal:
    default_negated_aggreagate
    | aggregate
    ;

default_negated_aggreagate:
    NOT aggregate
    ;

aggregate:
    term relational_op aggregate_function
        (relational_op term)?
    | aggregate_function relational_op term
    ;

aggregate_function:
    aggregate_function_symbol CURLY_OPEN
    (~CURLY_CLOSE aggregate_elements)? CURLY_CLOSE
    ;

aggregate_function_symbol:
    AGGREGATE_COUNT
    | AGGREGATE_MAX
    | AGGREGATE_MIN
    | AGGREGATE_SUM
    ;

aggregate_elements:
    aggregate_element (SEMICOLON aggregate_element)+ ;

aggregate_element:
    terms? (COLON (extended_literals)?)?
    ;

optimize:
    optimize_function CURLY_OPEN (optimize_elements)? CURLY_CLOSE DOT
    ;

optimize_function:
    MAXIMIZE
    | MINIMIZE ;

optimize_elements:
    optimize_element (SEMICOLON optimize_element)+ ;

optimize_element:
    weight_at_level (COLON (extended_literals)?)?
    ;

query:
    classical_literal QUERY_MARK
    ;

classical_literal:
    strong_negated_atom
    | atom
    ;

atom:
    predicate_symbol (PAREN_OPEN (terms)? PAREN_CLOSE)?
    ;

strong_negated_atom:
    op_=(MINUS | '¬') atom_=atom # strong_negated /* '¬' must be replaced for dlv */
    ;

predicate_symbol:
    id=ID # Id
	;

builtin_atom:
    arithmetic_atom /* not supported by dlv */
    | binary_relation
    ;
	
arithmetic_atom:
    (
        PLUS
        | MINUS
        | TIMES
        | DIV
        | '#int'
    )
    (PAREN_OPEN (terms)? PAREN_CLOSE)?
    ;
	
binary_relation:
    term relational_op term
    ;

extended_literals:
    extended_literal (COMMA extended_literal)+
    ;

extended_literal:
    default_negated_literal
    | modal_literal /* not supported by dlv */
    | classical_literal
    | builtin_atom
    ;

default_negated_literal:
    not_=NOT (mod_=modal_literal | cl_=classical_literal | builtin_atom_=builtin_atom) # default_negated_lit
    ;

modal_literal:
    k_modal
    | m_modal
    ;
	
k_modal:
    k_='K' literal_=classical_literal # k_mod
    ;

m_modal:
    m_='M' literal_=classical_literal # m_mod
    ;

	
weight_at_level:
    term (AT term)? (COMMA terms)?
    ;

terms:
    term (COMMA term)* ;

term:
    interval
    | arith_expr
    | basic_term
    ;

arith_expr:
    additive
    | multiplicative
    ;

additive:
    arith_term ((PLUS | MINUS) arith_term)+
    ;

arith_term:
    multiplicative
    | basic_term ;

multiplicative:
    arith_factor ((TIMES | DIV) arith_factor)+
    ;

arith_factor:
    basic_term ;

basic_term:
    subterm
    | negative_term
    | functional
    | constant
    | variable
    ;

interval:
    (arith_expr | basic_term) '..' term
    ;

functional:
    function_symbol PAREN_OPEN (terms)? PAREN_CLOSE
    ;

function_symbol:
    ID ;

subterm:
    PAREN_OPEN term PAREN_CLOSE
    ;

negative_term:
    MINUS term
    ;

constant:
    STRING
    | ID
    | NUMBER
    ;

variable:
    VARIABLE
    | ANONYMOUS_VARIABLE
    ;


relational_op:
    LESS_OR_EQ
    | GREATER_OR_EQ
    | UNEQUAL
    | EQUAL
    | LESS
    | GREATER
    ;

	
/* fragment NOT_AUX_ID
    : ~('a') ~('n') [a-z][a-zA-Z0-9_]*
    | ('a') ~('u') [a-zA-Z0-9_]*
    | ('au') ~('x') [a-zA-Z0-9_]*
	| ('aux') ~('_') [a-zA-Z0-9_]*
	| ('aux_') ~('_') [a-zA-Z0-9_]*
	| ('n') ~('o') [a-zA-Z0-9_]*
	| ('no') ~('t') [a-zA-Z0-9_]*
	| ('n'|'no'|'a'|'au'|'aux'|'aux_')
    ; */
	
fragment NOT_AUX_ID
    : [a-z][a-zA-Z0-9_]*
    ;

COMMENT :  '%' ~( '\r' | '\n' )* -> skip;
DOT: [.];
ANONYMOUS_VARIABLE : [_] ;
COMMA : [,] ;
QUERY_MARK : [?] ;
COLON : [:] ;
SEMICOLON : [;] ;
OR : [|] | [v]; /* '|' not supported by dlv */
NOT : 'not';
CONS : ':-' | [?];
WCONS : ':~' ;
PLUS : [+] ;
MINUS : [-] ;
TIMES : [*] ;
DIV : [/] ;
AT : [@] ;
PAREN_OPEN : [(] ;
PAREN_CLOSE : [)] ;
SQUARE_OPEN : '[' ;
SQUARE_CLOSE : ']' ;
CURLY_OPEN : [{] ;
CURLY_CLOSE : [}] ;
EQUAL : [=] ;
UNEQUAL : '<>' | '!=' ;
LESS : [<] ;
GREATER : [>] ;
LESS_OR_EQ : '<=' ;
GREATER_OR_EQ : '>=' ;
AGGREGATE_COUNT : '#count' ;
AGGREGATE_MAX : '#max' ;
AGGREGATE_MIN : '#min' ;
AGGREGATE_SUM : '#sum' ;
MINIMIZE : '#minimi' [zs] 'e' ;
MAXIMIZE : '#maximi' [zs] 'e' ;
ID : NOT_AUX_ID;
VARIABLE : [A-Z][a-zA-Z0-9_]* ;
STRING : '"' (~'"'|'\\"')* '"';
NUMBER : [0-9]+ ;
WS: [ \n\t\r]+ -> skip;