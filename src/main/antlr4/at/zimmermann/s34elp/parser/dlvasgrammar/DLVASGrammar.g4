grammar DLVASGrammar;

start:
    answersets EOF;

answersets:
    (CURLY_OPEN answerset CURLY_CLOSE)*
    ;

answerset:
    facts
    ;

facts:
	atom (COMMA atom)*
    ;

atom:
    predicate_symbol (PAREN_OPEN (terms)? PAREN_CLOSE)?
    ;

predicate_symbol:
    ID
	;

terms:
    term (COMMA term)* ;

term:
    interval
    | arith_expr
    | basic_term
    ;

arith_expr:
    additive
    | multiplicative
    ;

additive:
    arith_term ((PLUS | MINUS) arith_term)+
    ;

arith_term:
    multiplicative
    | basic_term ;

multiplicative:
    arith_factor ((TIMES | DIV) arith_factor)+
    ;

arith_factor:
    basic_term ;

basic_term:
    subterm
    | negative_term
    | functional
    | constant
    | variable
    ;

interval:
    (arith_expr | basic_term) '..' term
    ;

functional:
    function_symbol PAREN_OPEN (terms)? PAREN_CLOSE
    ;

function_symbol:
    ID ;

subterm:
    PAREN_OPEN term PAREN_CLOSE
    ;

negative_term:
    MINUS term
    ;

constant:
    STRING
    | ID
    | NUMBER
    ;

variable:
    VARIABLE
    | ANONYMOUS_VARIABLE
    ;


relational_op:
    LESS_OR_EQ
    | GREATER_OR_EQ
    | UNEQUAL
    | EQUAL
    | LESS
    | GREATER
    ;

fragment NOT_AUX_ID
    : [a-z][a-zA-Z0-9_]*
    ;
	
DOT: [.];
ANONYMOUS_VARIABLE : [_] ;
COMMA : [,] ;
QUERY_MARK : [?] ;
COLON : [:] ;
SEMICOLON : [;] ;
OR : [|] | [v]; /* '|' not supported by dlv */
NOT : 'not';
CONS : ':-' | [?];
WCONS : ':~' ;
PLUS : [+] ;
MINUS : [-] ;
TIMES : [*] ;
DIV : [/] ;
AT : [@] ;
PAREN_OPEN : [(] ;
PAREN_CLOSE : [)] ;
SQUARE_OPEN : '[' ;
SQUARE_CLOSE : ']' ;
CURLY_OPEN : [{] ;
CURLY_CLOSE : [}] ;
EQUAL : [=] ;
UNEQUAL : '<>' | '!=' ;
LESS : [<] ;
GREATER : [>] ;
LESS_OR_EQ : '<=' ;
GREATER_OR_EQ : '>=' ;
AGGREGATE_COUNT : '#count' ;
AGGREGATE_MAX : '#max' ;
AGGREGATE_MIN : '#min' ;
AGGREGATE_SUM : '#sum' ;
MINIMIZE : '#minimi' [zs] 'e' ;
MAXIMIZE : '#maximi' [zs] 'e' ;
ID : NOT_AUX_ID;
VARIABLE : [A-Z][a-zA-Z0-9_]* ;
STRING : '"' (~'"'|'\\"')* '"';
NUMBER : [0-9]+ ;
WS: [ \n\t\r]+ -> skip;