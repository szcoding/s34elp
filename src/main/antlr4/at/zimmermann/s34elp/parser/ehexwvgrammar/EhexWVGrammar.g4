grammar EhexWVGrammar;

start:
    (world_view)* EOF;

world_view:
    (NUMBER | AT)* epistemic_guess COLON answersets;

epistemic_guess:
    CURLY_OPEN (facts)? CURLY_CLOSE;

answersets:
    (answerset)*;

answerset:
    CURLY_OPEN (facts)? CURLY_CLOSE
    ;

facts:
	extended_literal (COMMA extended_literal)*
    ;

extended_literal:
    default_negated_literal
    | modal_literal
    | classical_literal
    ;

default_negated_literal:
    not_=NOT (mod_=modal_literal | cl_=classical_literal)
    ;

modal_literal:
    k_modal
    | m_modal
    ;

k_modal:
    k_='K' literal_=classical_literal # k_mod
    ;

m_modal:
    m_='M' literal_=classical_literal # m_mod
    ;


classical_literal:
    strong_negated_atom
    | atom
    ;

strong_negated_atom:
    (MINUS | '¬') atom
    ;

atom:
    predicate_symbol (PAREN_OPEN (terms)? PAREN_CLOSE)?
    ;

predicate_symbol:
    ID
	;

terms:
    term (COMMA term)* ;

term:
    interval
    | arith_expr
    | basic_term
    ;

arith_expr:
    additive
    | multiplicative
    ;

additive:
    arith_term ((PLUS | MINUS) arith_term)+
    ;

arith_term:
    multiplicative
    | basic_term ;

multiplicative:
    arith_factor ((TIMES | DIV) arith_factor)+
    ;

arith_factor:
    basic_term ;

basic_term:
    subterm
    | negative_term
    | functional
    | constant
    | variable
    ;

interval:
    (arith_expr | basic_term) '..' term
    ;

functional:
    function_symbol PAREN_OPEN (terms)? PAREN_CLOSE
    ;

function_symbol:
    ID ;

subterm:
    PAREN_OPEN term PAREN_CLOSE
    ;

negative_term:
    MINUS term
    ;

constant:
    STRING
    | ID
    | NUMBER
    ;

variable:
    VARIABLE
    | ANONYMOUS_VARIABLE
    ;


relational_op:
    LESS_OR_EQ
    | GREATER_OR_EQ
    | UNEQUAL
    | EQUAL
    | LESS
    | GREATER
    ;

fragment NOT_AUX_ID
    : [a-z][a-zA-Z0-9_]*
    ;

DOT: [.];
ANONYMOUS_VARIABLE : [_] ;
COMMA : [,] ;
QUERY_MARK : [?] ;
COLON : [:] ;
SEMICOLON : [;] ;
OR : [|] | [v]; /* '|' not supported by dlv */
NOT : 'not';
CONS : ':-' | [?];
WCONS : ':~' ;
PLUS : [+] ;
MINUS : [-] ;
TIMES : [*] ;
DIV : [/] ;
AT : [@] ;
PAREN_OPEN : [(] ;
PAREN_CLOSE : [)] ;
SQUARE_OPEN : '[' ;
SQUARE_CLOSE : ']' ;
CURLY_OPEN : [{] ;
CURLY_CLOSE : [}] ;
EQUAL : [=] ;
UNEQUAL : '<>' | '!=' ;
LESS : [<] ;
GREATER : [>] ;
LESS_OR_EQ : '<=' ;
GREATER_OR_EQ : '>=' ;
AGGREGATE_COUNT : '#count' ;
AGGREGATE_MAX : '#max' ;
AGGREGATE_MIN : '#min' ;
AGGREGATE_SUM : '#sum' ;
MINIMIZE : '#minimi' [zs] 'e' ;
MAXIMIZE : '#maximi' [zs] 'e' ;
ID : NOT_AUX_ID;
VARIABLE : [A-Z][a-zA-Z0-9_]* ;
STRING : '"' (~'"'|'\\"')* '"';
NUMBER : [0-9]+ ;
WS: [ \n\t\r]+ -> skip;
