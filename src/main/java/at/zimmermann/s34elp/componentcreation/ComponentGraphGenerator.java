package at.zimmermann.s34elp.componentcreation;

import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.component.ComponentGraph;
import at.zimmermann.s34elp.metaprogramtransformation.MetaSymbols;
import at.zimmermann.s34elp.parser.dlvasgrammar.DLVASGrammarLexer;
import at.zimmermann.s34elp.parser.dlvasgrammar.DLVASGrammarParser;
import at.zimmermann.s34elp.parser.dlvasgrammar.DLVASGrammarTreeVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class ComponentGraphGenerator {

    private String compPredicate = MetaSymbols.META_M_R2C;
    private String depPredicate = MetaSymbols.META_M_COMP_DEP;

    /**
     * Generates a component graph from the given program and answer sets
     * @param program input program
     * @param metaProgramResultString answer sets obtained by the asp solver for the meta program
     * @return component graph according to the dependencies between the rules
     */
    public ComponentGraph generate(Program program, String metaProgramResultString) {

        metaProgramResultString = metaProgramResultString.replace("Best model: ", "");

        CharStream input = CharStreams.fromString(metaProgramResultString);
        DLVASGrammarLexer lexer = new DLVASGrammarLexer(input);
        CommonTokenStream token = new CommonTokenStream(lexer);
        DLVASGrammarParser parser = new DLVASGrammarParser(token);
        ParseTree parseTree = parser.start();

        ComponentGraph componentGraph = (ComponentGraph) new DLVASGrammarTreeVisitor(program, compPredicate, depPredicate).visit(parseTree);

        return componentGraph;

    }

    public String getCompPredicate() {
        return compPredicate;
    }

    public void setCompPredicate(String compPredicate) {
        this.compPredicate = compPredicate;
    }

    public String getDepPredicate() {
        return depPredicate;
    }

    public void setDepPredicate(String depPredicate) {
        this.depPredicate = depPredicate;
    }
}
