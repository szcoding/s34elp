package at.zimmermann.s34elp.componentevaluation;

import at.zimmermann.s34elp.entity.Body;
import at.zimmermann.s34elp.entity.Head;
import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.Rule;
import at.zimmermann.s34elp.entity.atom.Literal;
import at.zimmermann.s34elp.entity.component.Component;
import at.zimmermann.s34elp.entity.component.ComponentGraph;
import at.zimmermann.s34elp.entity.solution.WorldView;
import at.zimmermann.s34elp.exception.ComponentEvaluationException;
import at.zimmermann.s34elp.exception.NoValidProgramException;
import at.zimmermann.s34elp.solver.ELPSolver;
import at.zimmermann.s34elp.solver.impl.EhexSolver;
import at.zimmermann.s34elp.util.RunTimeStatistics;
import at.zimmermann.s34elp.util.RunTimeStatisticsEntry;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ComponentEvaluation {

    private final static Logger LOGGER = Logger.getLogger(ComponentEvaluation.class);

    private RunTimeStatistics runTimeStatistics = null;
    private long startedAction;

    private final ELPSolver elpSolver;
    private boolean deleteTempFiles = true;

    public ComponentEvaluation() {
        elpSolver = new EhexSolver();
    }

    public ComponentEvaluation(RunTimeStatistics runTimeStatistics) {
        this();
        this.runTimeStatistics = runTimeStatistics;
    }

    /**
     * Evaluates componentgraph for a given input program
     * @param program input program
     * @param componentGraph to split up the program into components
     * @return worldviews of overall program, null if no worldviews are found
     * @throws ComponentEvaluationException
     */
    public List<WorldView> evaluateComponents(Program program, ComponentGraph componentGraph) throws ComponentEvaluationException {

        if (componentGraph.getOrderedComponentListForEvaluation() != null && componentGraph.getOrderedComponentListForEvaluation().size() > 0) {
            List<Component> orderedComponentList = componentGraph.getOrderedComponentListForEvaluation();
            return solveCompList(new WorldView(), 0, orderedComponentList, program);
        }

        return null;

    }

    /**
     * Evaluates components based on previous results
     * @param wvLastComponent worldview of the last component, null if this is the first component
     * @param curCompInd current index of the component list
     * @param componentList list containing all components
     * @param program input program
     * @return worldviews of overall program, null if no worldviews are found
     * @throws ComponentEvaluationException
     */
    private List<WorldView> solveCompList(WorldView wvLastComponent, int curCompInd, List<Component> componentList, Program program) throws ComponentEvaluationException {

        Program compProgram = new Program();
        List<WorldView> worldViews = null;

        for (int i = 0; i <= curCompInd; i++) {
            compProgram.addProgramRules(componentList.get(i).getSubstitutionProgram(program));
        }

        // add contraints obtained by last component / epistemic guess
        compProgram.addRules(getConstraintsFromEpGuess(wvLastComponent));

        if (!componentList.get(curCompInd).containsModalAtom(program) && (curCompInd < componentList.size() - 1)) {
            return solveCompList(wvLastComponent, curCompInd + 1, componentList, program);
        }

        /**
         * for the last subprogram, we need the epistemic guesses as well as the world views
         */
        if (curCompInd == componentList.size()-1) {
            elpSolver.onlyComputeEpistemicGuesses(false);
        } else {
            elpSolver.onlyComputeEpistemicGuesses(true);
        }

        try {

            // last component
            if (curCompInd == componentList.size() - 1 && runTimeStatistics != null) {
                startedAction = System.currentTimeMillis();
            }

            worldViews = elpSolver.solve(compProgram);

            // last component
            if (curCompInd == componentList.size() - 1 && runTimeStatistics != null) {
                runTimeStatistics.addEntry(new RunTimeStatisticsEntry("Evaluation last Component", System.currentTimeMillis() - startedAction));
            }

        } catch (NoValidProgramException ex) {
            throw new ComponentEvaluationException(ex);
        }

        /**
         * if there is no solution, we do not need to further evaluate the components
         * if the component index + 1 is equals to the number of components, we return the world views for the overall programm
         * else we have to evaluate the next component
         */
        if(worldViews == null || worldViews.isEmpty()) {
            return null;
        } else if ((curCompInd + 1) == componentList.size()) {
            return worldViews;
        } else {
            for (WorldView solution : worldViews) {
                return solveCompList(solution, curCompInd + 1, componentList, program);
            }
        }

        // this line should never be reached
        return null;

    }

    /**
     * Obtains constraints from epistemic guesses
     * @param worldView from which epistemic guess constraints will be obtained
     * @return list of constraints
     */
    private List<Rule> getConstraintsFromEpGuess(WorldView worldView) {

        List<Rule> constraints = new ArrayList<>();

        for (Literal epistemicGuess : worldView.getEpistemicGuesses()) {
            Rule constraint = new Rule();
            Body b = new Body();
            List<Literal> list = new ArrayList<>();
            Literal copy = epistemicGuess.copy();
            copy.setDefaultNegated(!copy.isDefaultNegated());
            list.add(copy);
            b.setBodyList(list);
            constraint.setHead(new Head());
            constraint.setBody(b);
            constraints.add(constraint);
        }

        return constraints;

    }

    /**
     * Indicates whether temporary files for evaluation will be deleted afterwards (for debugging purposes)
     * @param deleteTempFiles
     */
    public void deleteTempFiles(boolean deleteTempFiles) {
        this.deleteTempFiles = deleteTempFiles;
        elpSolver.deleteTempFiles(deleteTempFiles);
    }

}
