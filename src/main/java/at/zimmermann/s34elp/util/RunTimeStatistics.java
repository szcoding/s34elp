package at.zimmermann.s34elp.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RunTimeStatistics {

    private List<RunTimeStatisticsEntry> timeTable = new ArrayList<>();

    public void addEntry(RunTimeStatisticsEntry entry) {
        timeTable.add(entry);
    }

    public List<RunTimeStatisticsEntry> getTimeTable() {
        return timeTable;
    }

    public String dataToString() {
        return timeTable.stream().map(entry -> entry.getValue().toString()).collect(Collectors.joining(","));
    }

    @Override
    public String toString() {

        return timeTable.stream().map(entry -> entry.getName()).collect(Collectors.joining(",")) + "\n" +
                timeTable.stream().map(entry -> entry.getValue().toString()).collect(Collectors.joining(","));

    }

}
