package at.zimmermann.s34elp.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TempFileCreator {

    public static File createTempFile(String prefix, String suffix, String content) throws IOException {

        // create new temp file for meta program
        File tempFile = File.createTempFile(prefix, suffix, new File(".").getCanonicalFile());

        if (!tempFile.exists()) {
            tempFile.createNewFile();
        }

        FileWriter fileWriter = new FileWriter(tempFile);
        fileWriter.write(content);
        fileWriter.flush();
        fileWriter.close();

        return tempFile;
    }

}
