package at.zimmermann.s34elp.entity.term;

import java.util.Objects;

public class Constant extends BasicTerm {

    private String value;

    public Constant() {
    }

    public Constant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public Term copy() {
        Constant c = new Constant();
        c.value = new String(this.value);
        return c;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Constant)) return false;
        Constant constant = (Constant) o;
        return Objects.equals(value, constant.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return value;
    }


}
