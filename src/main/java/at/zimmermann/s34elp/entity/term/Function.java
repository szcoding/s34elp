package at.zimmermann.s34elp.entity.term;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class Function extends BasicTerm {

    private List<Term> arguments;
    private String id;

    public Function() {
        arguments = new ArrayList<>();
    }

    public void addArgument(Term argument) {
        arguments.add(argument);
    }

    public List<Term> getArguments() {
        return arguments;
    }

    public void setArguments(List<Term> arguments) {
        this.arguments = arguments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Term copy() {
        Function f = new Function();
        f.id = new String(id);
        List<Term> arg = new ArrayList<>();
        for (Term term : this.arguments) {
            arg.add(term.copy());
        }
        f.arguments = arg;
        return f;
    }

    @Override
    public LinkedHashSet<Variable> getVariables() {
        LinkedHashSet<Variable> variables = new LinkedHashSet<>();
        for (Term term : arguments) {
            variables.addAll(term.getVariables());
        }
        return variables;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Function)) return false;
        Function function = (Function) o;
        return Objects.equals(arguments, function.arguments) &&
                Objects.equals(id, function.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(arguments, id);
    }

    @Override
    public Term getSubstitute(final Map<String, String> substitutionMap) {
        Function f = new Function();
        f.id = this.id;
        List<Term> arg = new ArrayList<>();
        for (Term term : this.arguments) {
            arg.add(term.getSubstitute(substitutionMap));
        }
        f.arguments = arg;
        return f;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(id);
        if (arguments != null && arguments.size() > 0) {
            stringBuilder.append("(");
            stringBuilder.append(StringUtils.join(arguments,","));
            stringBuilder.append(")");
        }
        return stringBuilder.toString();
    }
}
