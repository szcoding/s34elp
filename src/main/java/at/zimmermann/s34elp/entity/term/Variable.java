package at.zimmermann.s34elp.entity.term;

import java.util.LinkedHashSet;
import java.util.Map;

public class Variable extends BasicTerm {

    private String name;

    public Variable() {

    }

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public LinkedHashSet<Variable> getVariables() {
        LinkedHashSet<Variable> variables = new LinkedHashSet<>();
        variables.add(this);
        return variables;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Term copy() {
        Variable v = new Variable();
        v.name = new String(this.name);
        return v;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Variable variable = (Variable) o;

        return name != null ? name.equals(variable.name) : variable.name == null;
    }

    @Override
    public Term getSubstitute(final Map<String, String> substitutionMap) {
        Variable v = new Variable();
        if (substitutionMap.containsKey(this.name)) {
            Constant c = new Constant();
            c.setValue(substitutionMap.get(this.name));
            return c;
        } else {
            v.name = this.name;
        }
        return v;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
