package at.zimmermann.s34elp.entity.atom;

import at.zimmermann.s34elp.entity.term.Term;
import at.zimmermann.s34elp.entity.term.Variable;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class Atom {

    private List<Term> arguments;
    private String id;

    public Atom() {
        arguments = new ArrayList<>();
    }

    public Atom copy() {
        Atom a = new Atom();
        a.id = new String(this.id);
        List<Term> arg = new ArrayList<>();
        for (Term term : this.arguments) {
            arg.add(term.copy());
        }
        a.arguments = arg;
        return a;
    }

    public LinkedHashSet<Variable> getVariables() {
        LinkedHashSet<Variable> variables = new LinkedHashSet<>();
        for (Term term : arguments) {
            variables.addAll(term.getVariables());
        }
        return variables;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Term> getArguments() {
        return arguments;
    }

    public void setArguments(List<Term> arguments) {
        this.arguments = arguments;
    }

    public void addArguments(Term argument) {
        this.arguments.add(argument);
    }

    public Atom getSubstitute(final Map<String, String> substitutionMap) {
        Atom a = new Atom();
        a.id = this.id;
        List<Term> arg = new ArrayList<>();
        for (Term term : arguments) {
            arg.add(term.getSubstitute(substitutionMap));
        }
        a.arguments = arg;
        return a;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Atom)) return false;
        Atom atom = (Atom) o;
        return Objects.equals(arguments, atom.arguments) &&
                Objects.equals(id, atom.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(arguments, id);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(id);
        if (arguments != null && arguments.size() > 0) {
            sb.append("(");
            sb.append(StringUtils.join(arguments,","));
            sb.append(")");
        }
        return sb.toString();
    }

    public boolean isBuiltin() {
        return false;
    }
}
