package at.zimmermann.s34elp.entity.atom;

import at.zimmermann.s34elp.entity.term.Variable;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;

public class Literal {

    private boolean isNegated;
    private boolean isDefaultNegated;
    private Atom atom;
    private MODAL modal;

    public Literal() {
        atom = null;
        isDefaultNegated = false;
        isNegated = false;
        modal = MODAL.NONE;
    }

    public Literal(Atom atom) {
        this.atom = atom;
        isDefaultNegated = false;
        isNegated = false;
        modal = MODAL.NONE;
    }

    public Literal copy() {
        Literal l = new Literal();
        l.isNegated = this.isNegated;
        l.isDefaultNegated = this.isDefaultNegated;
        l.modal = this.modal;
        l.atom = this.atom.copy();
        return l;
    }

    public boolean containsModalAtom() {
        return this.modal == MODAL.K || this.modal == MODAL.M;
    }

    public boolean isBuiltin() {
        return atom.isBuiltin();
    }

    public LinkedHashSet<Variable> getVariables() {
        return atom.getVariables();
    }

    public MODAL getModal() {
        return modal;
    }

    public void setModal(MODAL modal) {
        this.modal = modal;
    }

    public boolean isDefaultNegated() {
        return isDefaultNegated;
    }

    public void setDefaultNegated(boolean defaultNegated) {
        isDefaultNegated = defaultNegated;
    }

    public Atom getAtom() {
        return atom;
    }

    public void setAtom(Atom atom) {
        this.atom = atom;
    }

    public boolean isNegated() {
        return isNegated;
    }

    public void setNegated(boolean negated) {
        isNegated = negated;
    }

    public Literal getSubstitute(final Map<String, String> substitutionMap) {
        Literal l = new Literal();
        l.isNegated = this.isNegated;
        l.isDefaultNegated = this.isDefaultNegated;
        l.modal = this.modal;
        l.atom = atom.getSubstitute(substitutionMap);
        return l;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Literal)) return false;
        Literal literal = (Literal) o;
        return isNegated == literal.isNegated &&
                Objects.equals(atom, literal.atom) &&
                modal == literal.modal;
    }

    @Override
    public int hashCode() {

        return Objects.hash(isNegated, atom, modal);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (isDefaultNegated) {
            stringBuilder.append("not ");
        }

        switch (modal) {
            case NONE: {
                break;
            }
            default: {
                stringBuilder.append(modal.name() + " ");
            }
        }

        if (isNegated) {
            stringBuilder.append("-");
        }

        stringBuilder.append(atom);

        return stringBuilder.toString();
    }

    public enum MODAL {
        K, M, NONE
    }
}
