package at.zimmermann.s34elp.entity.atom;

import at.zimmermann.s34elp.entity.term.Term;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BuiltinAtom extends Atom {

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.getArguments() != null && this.getArguments().size() > 0) {
            stringBuilder.append(this.getArguments().get(0));
            stringBuilder.append(" " + this.getId() + " ");
            stringBuilder.append(this.getArguments().get(1));
        }
        return stringBuilder.toString();
    }

    @Override
    public Atom getSubstitute(final Map<String, String> substitutionMap) {
        BuiltinAtom a = new BuiltinAtom();
        a.setId(new String(this.getId()));
        List<Term> arg = new ArrayList<>();
        for (Term term : this.getArguments()) {
            arg.add(term.getSubstitute(substitutionMap));
        }
        a.setArguments(arg);
        return a;
    }

    @Override
    public BuiltinAtom copy() {
        BuiltinAtom a = new BuiltinAtom();
        a.setId(new String(this.getId()));
        List<Term> arg = new ArrayList<>();
        for (Term term : this.getArguments()) {
            arg.add(term.copy());
        }
        a.setArguments(arg);
        return a;
    }

    @Override
    public boolean isBuiltin() {
        return true;
    }
}
