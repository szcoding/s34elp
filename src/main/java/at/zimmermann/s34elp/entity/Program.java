package at.zimmermann.s34elp.entity;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Program {

    private Map<Integer, Rule> rules;

    public Program() {
        rules = new HashMap<>();
    }

    public void addRule(Integer number, Rule rule) {
        rules.put(number, rule);
    }

    public void addRule(Rule rule) {
        rules.put(rules.keySet().stream().max(Comparator.naturalOrder()).orElse(new Integer(0)).intValue() + 1, rule);
    }

    public void addRules(Collection<Rule> rules) {
        for (Rule rule : rules) {
            addRule(rule);
        }
    }

    public Map<Integer, Rule> getRules() {
        return rules;
    }

    public void addProgramRules(Program program) {
        for (Rule r : program.rules.values()) {
            addRule(r);
        }
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        for (Map.Entry<Integer, Rule> entry : rules.entrySet()) {
            stringBuilder.append(entry.getValue() + "\n");
        }

        return stringBuilder.toString();
    }

}
