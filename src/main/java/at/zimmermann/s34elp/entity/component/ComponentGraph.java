package at.zimmermann.s34elp.entity.component;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.log4j.Logger;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;

import java.util.Collections;
import java.util.List;
import java.util.stream.StreamSupport;

public class ComponentGraph {

    private static final Logger LOGGER = Logger.getLogger(ComponentGraph.class);
    private DirectedAcyclicGraph<Component, DefaultEdge> graph = new DirectedAcyclicGraph<Component, DefaultEdge>(DefaultEdge.class);

    public void addComponent(Component component) {
        graph.addVertex(component);
    }

    /*
     Since the topological ordering is in reverse order of evaluation, the ordering has to be reversed
     */
    public List<Component> getOrderedComponentListForEvaluation() {
        List<Component> list = IteratorUtils.toList(graph.iterator());
        Collections.reverse(list);
        return list;
    }

    public Component getComponentByName(String compName) {

        return StreamSupport.stream(graph.spliterator(), false)
                .filter(c -> c.getCompName().equals(compName))
                .findFirst()
                .orElse(null);

    }

    public void addDependency(final Component comp1, final Component comp2) {

        try {
            graph.addEdge(comp1, comp2);
        } catch (IllegalArgumentException ex) {
            LOGGER.error(ex.getMessage() + "\n" + comp1 + "\n" + comp2);
        }

    }

    @Override
    public String toString() {
        return "ComponentGraph{" +
                "componentList=" + getOrderedComponentListForEvaluation() +
                '}';
    }
}
