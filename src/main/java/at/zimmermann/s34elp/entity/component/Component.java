package at.zimmermann.s34elp.entity.component;

import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.Rule;

import java.util.*;

public class Component {

    private String compName;

    // A map of rule numbers and the necessary variable substitutions
    private Map<Integer, List<Map<String, String>>> variableSubstitutions = new HashMap<>();

    public Component() {    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public Map<Integer, List<Map<String, String>>> getVariableSubstitutions() {
        return variableSubstitutions;
    }

    public void addRule(Integer ruleNo) {
        if (!variableSubstitutions.containsKey(ruleNo))
            variableSubstitutions.put(ruleNo, new ArrayList<>());
    }

    public void addSubstitution(Integer ruleNo, Map<String, String> substitution) {
        variableSubstitutions.get(ruleNo).add(substitution);
    }

    public Program getSubstitutionProgram(Program inputProgram) {

        Program outputProgram = new Program();

        for (Integer rule : variableSubstitutions.keySet()) {
            for (Map<String, String> substitution : variableSubstitutions.get(rule)) {
                Rule newRule = inputProgram.getRules().get(rule).getSubstitute(substitution);
                outputProgram.addRule(newRule);
            }
        }

        return outputProgram;
    }

    public boolean containsModalAtom(Program program) {
        for (Integer rule : variableSubstitutions.keySet()) {
            if (program.getRules().get(rule).containsModalAtom()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Component{" +
                "compName='" + compName + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Component)) return false;
        Component component = (Component) o;
        return Objects.equals(compName, component.compName);
    }

}
