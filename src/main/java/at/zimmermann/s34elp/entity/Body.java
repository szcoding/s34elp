package at.zimmermann.s34elp.entity;

import at.zimmermann.s34elp.entity.atom.Literal;
import at.zimmermann.s34elp.entity.term.Variable;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public class Body {

    private List<Literal> bodyList;

    public Body() {

        bodyList = new ArrayList<>();

    }

    public Body(List<Literal> bodyList) {
        this.bodyList = bodyList;
    }

    public List<Literal> getLiterals() {
        return bodyList;
    }

    public void setBodyList(List<Literal> bodyList) {
        this.bodyList = bodyList;
    }

    public boolean isEmpty() {
        return bodyList == null || bodyList.size() == 0;
    }

    public LinkedHashSet<Variable> getVariables() {
        LinkedHashSet<Variable> variables = new LinkedHashSet<>();
        for (Literal literal : bodyList) {
            variables.addAll(literal.getVariables());
        }
        return variables;
    }

    public Body getSubstitute(Map<String, String> substitutionMap) {
        Body b = new Body();
        List<Literal> literals = new ArrayList<>();
        for (Literal literal : bodyList) {
            literals.add(literal.getSubstitute(substitutionMap));
        }
        b.bodyList = literals;
        return b;
    }

    public List<Literal> getPositiveBody() {
        List<Literal> pBody = new ArrayList<>();
        for (Literal l : this.getLiterals()) {
            if (!l.isDefaultNegated()) {
                pBody.add(l);
            }
        }
        return pBody;
    }

    public List<Literal> getNegativeBody() {
        List<Literal> nBody = new ArrayList<>();
        for (Literal l : this.getLiterals()) {
            if (l.isDefaultNegated()) {
                nBody.add(l);
            }
        }
        return nBody;
    }

    @Override
    public String toString() {

        return StringUtils.join(bodyList,",");

    }

}
