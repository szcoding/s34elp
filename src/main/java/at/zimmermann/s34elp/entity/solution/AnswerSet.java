package at.zimmermann.s34elp.entity.solution;

import at.zimmermann.s34elp.entity.atom.Literal;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class AnswerSet {

    private List<Literal> answerSets;

    public AnswerSet() {
        answerSets = new ArrayList<>();
    }

    public List<Literal> getAnswerSets() {
        return answerSets;
    }

    public void setAnswerSets(List<Literal> answerSets) {
        this.answerSets = answerSets;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(StringUtils.join(answerSets, ","));
        sb.append("}");
        return sb.toString();
    }
}
