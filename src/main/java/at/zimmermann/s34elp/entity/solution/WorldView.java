package at.zimmermann.s34elp.entity.solution;

import at.zimmermann.s34elp.entity.atom.Literal;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class WorldView {

    private Collection<Literal> epistemicGuesses;
    private Collection<AnswerSet> answerSets;

    public WorldView() {
        epistemicGuesses = new HashSet<>();
        answerSets = new ArrayList<>();
    }

    public Collection<Literal> getEpistemicGuesses() {
        return epistemicGuesses;
    }

    public Collection<AnswerSet> getAnswerSets() {
        return answerSets;
    }

    public void addEpistemicGuess(Collection<Literal> assumptions) {
        if(assumptions != null)
            this.epistemicGuesses.addAll(assumptions);
    }

    public void addAnswerSet(AnswerSet answerset) {
        this.answerSets.add(answerset);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("WorldView ");
        sb.append("{");
        sb.append(StringUtils.join(epistemicGuesses,","));
        sb.append("}\n");
        sb.append("{");
        sb.append(StringUtils.join(answerSets, ",\n"));
        sb.append("}");
        return sb.toString();
    }
}
