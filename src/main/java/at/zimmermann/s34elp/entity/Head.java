package at.zimmermann.s34elp.entity;

import at.zimmermann.s34elp.entity.atom.Literal;
import at.zimmermann.s34elp.entity.term.Variable;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public class Head {

    private List<Literal> head;

    public Head() {
        head = new ArrayList<>();
    }

    public Head(List<Literal> head) {
        this.head = head;
    }

    public void addHeadLiteral(Literal literal) {
        head.add(literal);
    }

    public void setHead(List<Literal> head) {
        this.head = head;
    }

    public boolean isEmpty() {
        return head == null || head.size() == 0;
    }

    public List<Literal> getLiterals() {
        return head;
    }

    public LinkedHashSet<Variable> getVariables() {
        LinkedHashSet<Variable> variables = new LinkedHashSet<>();
        for (Literal literal : head) {
            variables.addAll(literal.getVariables());
        }
        return variables;
    }

    public Head getSubstitute(Map<String, String> substitutionMap) {
        Head h = new Head();
        List<Literal> literals = new ArrayList<>();
        for (Literal literal : head) {
            literals.add(literal.getSubstitute(substitutionMap));
        }
        h.head = literals;
        return h;
    }

    @Override
    public String toString() {
       return StringUtils.join(head, " v ");
    }

}
