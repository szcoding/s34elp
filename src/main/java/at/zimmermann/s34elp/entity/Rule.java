package at.zimmermann.s34elp.entity;

import at.zimmermann.s34elp.entity.atom.Literal;
import at.zimmermann.s34elp.entity.term.Variable;

import java.util.LinkedHashSet;
import java.util.Map;

public class Rule {

    private Head head;
    private Body body;

    public Rule() {
    }

    public Rule(Head head, Body body) {
        this.head = head;
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public LinkedHashSet<Variable> getVariables() {
        LinkedHashSet<Variable> variables = new LinkedHashSet<>();
        if (head != null) {
            variables.addAll(head.getVariables());
        }
        if (body != null) {
            variables.addAll(body.getVariables());
        }
        return variables;
    }

    public Rule getSubstitute(Map<String, String> substitutionMap) {
        Rule r = new Rule();
        if (head != null)
            r.head = head.getSubstitute(substitutionMap);
        if (body != null)
            r.body = body.getSubstitute(substitutionMap);
        return r;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        if (head != null && !head.isEmpty()) {
            stringBuilder.append(head);
        }

        if (body != null && !body.isEmpty()) {
            stringBuilder.append(":- " + body);
        }

        stringBuilder.append(".");
        return stringBuilder.toString();
    }

    public boolean containsModalAtom() {

        if (body == null) {
            return false;
        }

        for (Literal literal : body.getLiterals()) {
            if (literal.containsModalAtom()) {
                return true;
            }
        }
        return false;
    }

    public Rule copy() {
        return new Rule(head, body);
    }
}
