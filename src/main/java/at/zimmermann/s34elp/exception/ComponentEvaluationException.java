package at.zimmermann.s34elp.exception;

public class ComponentEvaluationException extends Exception{

    public ComponentEvaluationException() {}

    public ComponentEvaluationException(String message) {
        super(message);
    }

    public ComponentEvaluationException(Exception ex) {
        super(ex);
    }

}
