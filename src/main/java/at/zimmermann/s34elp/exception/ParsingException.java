package at.zimmermann.s34elp.exception;

public class ParsingException extends Exception {

    public ParsingException() {}

    public ParsingException(String message) {
        super(message);
    }
}
