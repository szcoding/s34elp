package at.zimmermann.s34elp.exception;

public class NoValidProgramException extends Exception {

    public NoValidProgramException() {}

    public NoValidProgramException(String message) {
        super(message);
    }

    public NoValidProgramException(Exception ex) { super(ex);}

}
