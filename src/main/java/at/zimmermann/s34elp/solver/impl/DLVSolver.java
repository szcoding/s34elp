package at.zimmermann.s34elp.solver.impl;

import at.zimmermann.s34elp.exception.NoValidProgramException;
import at.zimmermann.s34elp.solver.ASPSolver;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Evaluates answer set program
 */
public class DLVSolver implements ASPSolver {

    private final static Logger LOGGER = Logger.getLogger(DLVSolver.class);

    private List<String> filter = null;
    private Integer numberOfSolutions = null;

    public DLVSolver() {}

    public DLVSolver(Integer numberOfSolutions) {
        this.numberOfSolutions = numberOfSolutions;
    }

    @Override
    public String solve(File programFile) throws NoValidProgramException {

        List<String> commands = new ArrayList<>();

        commands.add("dlv");
        commands.add("-silent");

        if (filter != null) {
            commands.add("-pfilter=" + String.join(",", filter));
        }

        if (numberOfSolutions != null && numberOfSolutions > 0) {
            commands.add("-n=" + numberOfSolutions);
        }

        commands.add(programFile.getPath());

        try {
            LOGGER.debug("dlv: solving '" + StringUtils.join(commands, " ") + "'");
            Process dlvProc = new ProcessBuilder(commands).start();
            String output = IOUtils.toString(dlvProc.getInputStream(), "UTF-8");

            // remove additional information for weak constraints
            if (output.startsWith("Best entity: ")) {
                output = output.replaceFirst("Best model: ", "");
            }

            if (output.contains("Cost")) {
                output = output.substring(0, output.indexOf("Cost"));
            }

            LOGGER.debug(output);

            return output;
        } catch(IOException ex) {
            throw new NoValidProgramException(ex.getMessage());
        }

    }

    @Override
    public void setPredicateFilter(List<String> predicates) {
        this.filter = predicates;
    }

}
