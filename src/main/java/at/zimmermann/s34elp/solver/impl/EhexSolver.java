package at.zimmermann.s34elp.solver.impl;

import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.solution.WorldView;
import at.zimmermann.s34elp.exception.NoValidProgramException;
import at.zimmermann.s34elp.parser.ehexwvgrammar.EhexWVGrammarLexer;
import at.zimmermann.s34elp.parser.ehexwvgrammar.EhexWVGrammarParser;
import at.zimmermann.s34elp.parser.ehexwvgrammar.EhexWVGrammarTreeVisitor;
import at.zimmermann.s34elp.solver.ELPSolver;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class EhexSolver implements ELPSolver {

    /**
     * Evaluates world view of components
     */

    private final static Logger LOGGER = Logger.getLogger(EhexSolver.class);

    private final String pathToEhex = "ehex";
    private static int tempFileCounter = 1;
    private boolean onlyEpGuesses = false;
    private boolean deleteFiles = true;

    public List<WorldView> solve(Program program) throws NoValidProgramException {

        List<WorldView> worldViews = null;

        try {

            File tempFile = createFileFromProgram(program);
            if (tempFile != null) {
                worldViews = evaluate(tempFile);
            }
            if(deleteFiles) {
                tempFile.delete();
            }
        } catch (IOException ex) {
            throw new NoValidProgramException(ex);
        }

        return worldViews;

    }

    @Override
    public void onlyComputeEpistemicGuesses(boolean onlyEpGuess) {
        this.onlyEpGuesses = onlyEpGuesses;
    }

    @Override
    public void deleteTempFiles(boolean delete) {
        this.deleteFiles = delete;
    }


    public List<WorldView> evaluate(File file) throws IOException {

        LOGGER.debug("starting evaluation..");

        Process ehex = new ProcessBuilder(pathToEhex, file.getCanonicalPath()).start();
        String result = IOUtils.toString(ehex.getInputStream(), "UTF-8");
        result = result.replace("World view", "");
        result = result.replace(" wrt ", "");
        return parseWorldViews(result);

    }


    private List<WorldView> parseWorldViews(String result) throws IOException {

        CharStream input = CharStreams.fromString(result);
        EhexWVGrammarLexer lexer = new EhexWVGrammarLexer(input);
        CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
        EhexWVGrammarParser parser = new EhexWVGrammarParser(commonTokenStream);
        ParseTree parseTree = parser.start();

        EhexWVGrammarTreeVisitor visitor = new EhexWVGrammarTreeVisitor();
        visitor.setOnlyEpistemicGuesses(onlyEpGuesses);
        return (List<WorldView>) visitor.visit(parseTree);

    }

    private File createFileFromProgram(Program program) throws IOException {

        File tempFile = new File("elp_temp" + tempFileCounter++ + ".ehex");

        if (tempFile.exists()) {
            tempFile.delete();
            LOGGER.debug("Deleting file 'elp_temp" + tempFileCounter + ".ehex'");
        }

        if (!tempFile.exists()) {
            tempFile.createNewFile();
            LOGGER.debug("Creating new file 'elp_temp" + tempFileCounter + ".ehex'");
        }

        FileWriter fw = new FileWriter(tempFile);

        fw.append(program.toString());
        fw.flush();
        fw.close();

        return tempFile;


    }


}
