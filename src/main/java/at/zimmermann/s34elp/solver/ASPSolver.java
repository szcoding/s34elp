package at.zimmermann.s34elp.solver;

import at.zimmermann.s34elp.exception.NoValidProgramException;

import java.io.File;
import java.util.List;

public interface ASPSolver {

    /**
     * Returns the answer sets for a given answer set program
     * @param programFile file, that contains program that is evaluated
     * @return list of answer sets, empty list if there are no answer sets
     * @throws NoValidProgramException, if the program is not a valid asp program
     */
    public String solve(File programFile) throws NoValidProgramException;

    /**
     * Answer sets should only contain the given predicates
     * @param predicates to be filtered
     */
    public void setPredicateFilter(List<String> predicates);

}
