package at.zimmermann.s34elp.solver;

import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.solution.WorldView;
import at.zimmermann.s34elp.exception.NoValidProgramException;

import java.util.List;

public interface ELPSolver {

    /**
     * Returns the world views for a given epistemic logic program
     * @param program to be evaluated
     * @return list of world views, empty list if there are no world views
     * @throws NoValidProgramException, if the program is not a valid elp program
     */
    public List<WorldView> solve(Program program) throws NoValidProgramException;

    /**
     * Only epistemic guesses should be returned
     * @param onlyEpGuess
     */
    public void onlyComputeEpistemicGuesses(boolean onlyEpGuess);

    /**
     * States whether temporary files for computation will be deleted
     * @param delete
     */
    public void deleteTempFiles(boolean delete);

}
