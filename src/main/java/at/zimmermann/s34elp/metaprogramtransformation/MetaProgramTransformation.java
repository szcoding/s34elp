package at.zimmermann.s34elp.metaprogramtransformation;

import at.zimmermann.s34elp.entity.Body;
import at.zimmermann.s34elp.entity.Head;
import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.Rule;
import at.zimmermann.s34elp.entity.atom.Atom;
import at.zimmermann.s34elp.entity.atom.Literal;
import at.zimmermann.s34elp.entity.term.Function;
import at.zimmermann.s34elp.entity.term.Variable;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static at.zimmermann.s34elp.metaprogramtransformation.MetaSymbols.*;

/**
 * Creates meta-program from given program
 */

public class MetaProgramTransformation {

    private final static Logger LOGGER = Logger.getLogger(MetaProgramTransformation.class);

    private String compPredicate = MetaSymbols.META_R2C;
    private String depPredicate = MetaSymbols.META_COMP_DEP;

    /**
     * Generates meta program in a file for given input program
     * @param program valid epistemic logic program
     * @return String containing the meta program
     * @throws IOException
     */
    public StringBuilder generateMetaProgram(Program program) {

        StringBuilder sb = new StringBuilder();
        sb.append(getGroundingRules(program));
        sb.append(getDependencyRules());
        sb.append(getComponentRules());
        return sb;

    }

    public List<String> getCompPredicates() {
        List<String> predicates = new ArrayList<>();
        predicates.add(compPredicate);
        predicates.add(depPredicate);
        return predicates;
    }

    public String getGroundingRules(Program program) {

        Program metaProgram = new Program();

        // counter for meta program rule numbers
        int ruleNo = 0;

        // adds a new rule for each atom in the head of each rule
        for (Map.Entry<Integer, Rule> ruleEntry : program.getRules().entrySet()) {

            Rule ruleProgramm = ruleEntry.getValue();
            Head headProgramm = ruleProgramm.getHead();
            Body bodyProgramm = ruleProgramm.getBody();

            // adds rule for each head atom
            if (headProgramm != null && !headProgramm.isEmpty()) {
                for (Literal headLiteral : headProgramm.getLiterals()) {

                    Atom metaAtom = new Atom();
                    metaAtom.setId(MetaSymbols.META_HEAD);

                    Function metaFunction = new Function();
                    metaFunction.setId(MetaSymbols.META_RULE + ruleEntry.getKey());
                    metaFunction.setArguments(new ArrayList(ruleProgramm.getVariables()));

                    metaAtom.addArguments(metaFunction);
                    metaAtom.addArguments(convertLiteralToFunction(headLiteral));

                    Literal metaHeadLiteral = new Literal(metaAtom);
                    Head metaHead = new Head(Arrays.asList(metaHeadLiteral));

                    List<Literal> bodyList = new ArrayList<>();

                    int metaVarCounter = 1;

                    if (bodyProgramm != null) {
                        for (Literal pBodyLit : bodyProgramm.getPositiveBody()) {
                            if (pBodyLit.containsModalAtom() || pBodyLit.isBuiltin()) {
                                continue;
                            }
                            Atom pBAtom = new Atom();
                            pBAtom.setId(MetaSymbols.META_HEAD);
                            pBAtom.addArguments(new Variable(MetaSymbols.META_VARIABLE + (metaVarCounter++)));
                            pBAtom.addArguments(convertLiteralToFunction(pBodyLit));
                            bodyList.add(new Literal(pBAtom));
                        }
                    }

                    Body metaBody = new Body(bodyList);
                    Rule metaRule = new Rule(metaHead, metaBody);
                    metaProgram.addRule(ruleNo++, metaRule);
                }
            }

            // adds a new rule for each atom in positive body of each rule
            if (bodyProgramm != null && !bodyProgramm.getLiterals().isEmpty()) {
                for (Literal bodyLiteral : bodyProgramm.getLiterals()) {

                    if (bodyLiteral.isBuiltin())
                        continue;

                    Atom metaAtom = new Atom();
                    metaAtom.setId(MetaSymbols.META_BODY);

                    Function metaFunction = new Function();
                    metaFunction.setId(MetaSymbols.META_RULE + ruleEntry.getKey());
                    metaFunction.setArguments(new ArrayList(ruleProgramm.getVariables()));

                    metaAtom.addArguments(metaFunction);
                    metaAtom.addArguments(convertLiteralToFunction(bodyLiteral));

                    Literal metaHeadLiteral = new Literal(metaAtom);
                    Head metaHead = new Head(Arrays.asList(metaHeadLiteral));

                    List<Literal> bodyList = new ArrayList<>();

                    int metaVarCounter = 1;

                    for (Literal pBodyLit : bodyProgramm.getPositiveBody()) {
                        if (pBodyLit.containsModalAtom() || pBodyLit.isBuiltin()) {
                            continue;
                        }
                        Atom pBAtom = new Atom();
                        pBAtom.setId(MetaSymbols.META_HEAD);
                        pBAtom.addArguments(new Variable(MetaSymbols.META_VARIABLE + (metaVarCounter++)));
                        pBAtom.addArguments(convertLiteralToFunction(pBodyLit));
                        bodyList.add(new Literal(pBAtom));
                    }

                    Body metaBody = new Body(bodyList);
                    Rule metaRule = new Rule(metaHead, metaBody);
                    metaProgram.addRule(ruleNo++, metaRule);
                }
            }


        }

        return metaProgram.toString();

    }


    /**
     * helper function for converting a literal into a function, having the same parameters
     * @param l input literal
     * @return new function
     */
    private Function convertLiteralToFunction(Literal l) {

        Function f = new Function();

        if (l.isNegated()) {
            f.setId(META_NOT + l.getAtom().getId());
        } else {
            f.setId(l.getAtom().getId());
        }

        f.setArguments(l.getAtom().getArguments());

        return f;

    }

    public String getComponentRules() {

        StringBuilder compRules = new StringBuilder();

        compRules.append(META_SM_REACH + "(X) :- " + META_DEPENDENCY + "(X,Y), " + META_DEPENDENCY + "(Y,X), Y<X.\n");
        compRules.append(META_NOT_SM_REACH + "(X) :- " + META_DEPENDENCY + "(X,_), not " + META_SM_REACH + "(X).\n");
        compRules.append(META_COMP + "(X,Y) :- " + META_NOT_SM_REACH + "(X), " + META_DEPENDENCY + "(X,Y), " + META_DEPENDENCY + "(Y,X).\n");
        compRules.append(META_COMP + "(X,X) :- " + META_NOT_SM_REACH + "(X).\n");
        compRules.append(META_COMP_DEP + "(C1,C2) :- " + META_COMP + "(C1,X)," + META_COMP + "(C2,Y), C1!=C2, " + META_DEPENDENCY + "(X,Y), X!=Y.\n");
        compRules.append(META_R2C + "(C,R) :- " + META_COMP + "(C,X), " + META_HEAD + "(R,X).\n");

        return compRules.toString();

    }

    public String getDependencyRules() {

        StringBuilder depRules = new StringBuilder();

        depRules.append(META_DEPENDENCY + "(X,Y) :- " + META_HEAD + "(R,X), " + META_HEAD + "(R,Y).\n");
        depRules.append(META_DEPENDENCY + "(X,Y) :- " + META_HEAD + "(R,X), " + META_BODY + "(R,Y).\n");
        depRules.append(META_DEPENDENCY + "(X,Y) :- " + META_DEPENDENCY + "(X,Z), " + META_DEPENDENCY + "(Z,Y), X!=Y.\n");


        return depRules.toString();

    }

    public String getCompPredicate() {
        return compPredicate;
    }

    public String getDepPredicate() {
        return depPredicate;
    }
}
