package at.zimmermann.s34elp.metaprogramtransformation;

public class MetaSymbols {

    /**
     * predicate symbols for meta program
     */

    public static final String META_HEAD = "meta__head";
    public static final String META_BODY = "meta__body";
    public static final String META_VARIABLE = "META__R";
    public static final String META_DEPENDENCY = "meta__dep";
    public static final String META_SM_REACH = "meta__smreach";
    public static final String META_NOT_SM_REACH = "meta__notsmreach";
    public static final String META_COMP = "meta__comp";
    public static final String META_COMP_DEP = "meta__comp__dep";
    public static final String META_R2C = "meta__r2comp";

    public static final String META_M_COMP = "meta__m_comp";
    public static final String META_M_SOME_COMP = "meta__m_scomp";
    public static final String META_M_COMP_DEP = "meta__m_comp_dep";
    public static final String META_M_R2C = "meta__m_r2comp";
    public static final String META_M_COUNT = "meta__m_count";

    public static final String META_RULE = "r__";
    public static final String META_NOT = "not__";
    public static final String META_M = "m__";
    public static final String META_K = "k__";
    public static final String META_MODAL_COMP = "meta__modal_comp";

    public static final String META_AS = "as__";

}
