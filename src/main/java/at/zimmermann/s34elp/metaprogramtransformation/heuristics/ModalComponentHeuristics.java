package at.zimmermann.s34elp.metaprogramtransformation.heuristics;

import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.Rule;
import org.apache.log4j.Logger;

import static at.zimmermann.s34elp.metaprogramtransformation.MetaSymbols.*;

public class ModalComponentHeuristics implements Heuristics {

    private final static Logger LOGGER = Logger.getLogger(ModalComponentHeuristics.class);

    @Override
    public String apply(Program program) {

        LOGGER.debug("applying modalToMetaComp heuristic..");

        StringBuilder modalToMetaComp = new StringBuilder();

        boolean atLeastOneComp = false;

        for (Integer ruleNo : program.getRules().keySet()) {

            Rule rule = program.getRules().get(ruleNo);
            if (rule.containsModalAtom()) {
                atLeastOneComp = true;
                modalToMetaComp.append(META_MODAL_COMP + "(C) :- " + META_R2C + "(C," + META_RULE + ruleNo);
                if (!rule.getVariables().isEmpty()) {
                    modalToMetaComp.append("(_)");
                }
                modalToMetaComp.append(").\n");
            }

        }

        if (!atLeastOneComp) {
            modalToMetaComp.append(META_M_COMP + "(as,C) :- " + META_COMP + "(C,_).\n");
        }

        modalToMetaComp.append(META_M_COMP + "(C,C) :- " + META_COMP + "(C,X), " + META_MODAL_COMP + "(C).\n");

        return modalToMetaComp.toString();
    }
}
