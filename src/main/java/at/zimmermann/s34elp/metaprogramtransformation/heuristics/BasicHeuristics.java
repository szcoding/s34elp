package at.zimmermann.s34elp.metaprogramtransformation.heuristics;

import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.metaprogramtransformation.MetaSymbols;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static at.zimmermann.s34elp.metaprogramtransformation.MetaSymbols.*;


public class BasicHeuristics implements Heuristics {

    private final static Logger LOGGER = Logger.getLogger(BasicHeuristics.class);

    private String compPredicate = MetaSymbols.META_M_R2C;
    private String depPredicate = MetaSymbols.META_M_COMP_DEP;

    public List<String> getCompPredicates() {
        List<String> predicates = new ArrayList<>();
        predicates.add(compPredicate);
        predicates.add(depPredicate);
        return predicates;
    }

    public String getCompPredicate() {
        return compPredicate;
    }

    public String getDepPredicate() {
        return depPredicate;
    }


    @Override
    public String apply(Program program) {

        LOGGER.debug("applying basic heuristic..");

        StringBuilder metaCompRules = new StringBuilder();

        // guess
        metaCompRules.append(META_M_COMP + "(C,C) v -" + META_M_COMP + "(C,C) :- " +  META_COMP + "(C,_).\n");
        metaCompRules.append(META_M_COMP + "(MC,C) v -" + META_M_COMP + "(MC,C) :- " +  META_COMP + "(C,_), " + META_M_COMP + "(MC,_).\n");
        metaCompRules.append(META_M_COMP_DEP + "(MC1,MC2) :- " + META_M_COMP + "(MC1,C1)," + META_M_COMP + "(MC2,C2), " + META_COMP_DEP + "(C1,C2), MC1 != MC2.\n");

        // check
        // each component, has to be in exactly one meta component
        metaCompRules.append(META_M_SOME_COMP + "(C) :- " + META_COMP + "(C,_), " + META_M_COMP + "(MC,C).\n");
        metaCompRules.append(":- " +  META_COMP + "(C,_), not " + META_M_SOME_COMP + "(C).\n");
        metaCompRules.append(":- " + META_M_COMP + "(C1,X), " + META_M_COMP + "(C2,X), C1!=C2.\n");

        // no cyclic dependencies
        metaCompRules.append(":- " + META_M_COMP_DEP + "(C1,C2), " + META_M_COMP_DEP + "(C2,C1), C1!=C2.\n");

        // get rules for meta component
        metaCompRules.append(META_M_R2C + "(MC,R) :- " + META_M_COMP + "(MC,C)," + META_R2C + "(C,R).\n");


        return metaCompRules.toString();
    }
}
