package at.zimmermann.s34elp.metaprogramtransformation.heuristics;

import at.zimmermann.s34elp.entity.Program;

import static at.zimmermann.s34elp.metaprogramtransformation.MetaSymbols.META_M_COMP;
import static at.zimmermann.s34elp.metaprogramtransformation.MetaSymbols.META_M_COUNT;

public class SingleMetaCompHeuristics implements Heuristics {

    @Override
    public String apply(Program program) {

        StringBuilder sb = new StringBuilder();

        sb.append(META_M_COUNT + "(MC,1) :- " + META_M_COMP + "(MC,C).\n");
        sb.append(":~ " + META_M_COUNT + "(MC,C). [C:1]\n");

        return sb.toString();

    }
}
