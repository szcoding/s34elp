package at.zimmermann.s34elp.metaprogramtransformation.heuristics;

import at.zimmermann.s34elp.entity.Program;

public interface Heuristics {

    /**
     * Apply heuristic to input program
     * @param program input program
     * @return heuristic rules as a string
     */
    public String apply(Program program);

}
