package at.zimmermann.s34elp.metaprogramtransformation.heuristics;

import at.zimmermann.s34elp.entity.Program;

import static at.zimmermann.s34elp.metaprogramtransformation.MetaSymbols.*;

public class OnlyModalComponentHeuristics extends ModalComponentHeuristics {

    @Override
    public String apply(Program program) {

        StringBuilder sb = new StringBuilder();
        sb.append(super.apply(program));

        sb.append(":- " + META_M_COMP + "(C,_)," + META_COMP + "(C,_), not " + META_MODAL_COMP + "(C).\n");

        return sb.toString();
    }

}
