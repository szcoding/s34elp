package at.zimmermann.s34elp;

import at.zimmermann.s34elp.metaprogramtransformation.heuristics.Heuristics;
import at.zimmermann.s34elp.metaprogramtransformation.heuristics.ModalComponentHeuristics;
import at.zimmermann.s34elp.metaprogramtransformation.heuristics.OnlyModalComponentHeuristics;
import at.zimmermann.s34elp.metaprogramtransformation.heuristics.SingleMetaCompHeuristics;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class S34ELP {

    private static final Logger LOGGER = Logger.getLogger(S34ELP.class);

    public static long startTime;

    public static void main(String[] args) throws IOException {

        startTime = System.currentTimeMillis();

        Options options = new Options();

        Option fileOption = Option.builder("f")
                .longOpt("file")
                .required(true)
                .hasArg()
                .desc("file to be evaluated")
                .build();
        fileOption.setRequired(true);
        options.addOption(fileOption);

        Option showTempFiles = Option.builder("s")
                .hasArg(false)
                .required(false)
                .desc("output temp files")
                .longOpt("show")
                .build();
        options.addOption(showTempFiles);

        Option saveRuntimeStatistics = Option.builder("r")
                .hasArg(true)
                .required(false)
                .desc("saves runtime statistics to given file")
                .longOpt("rsave")
                .build();
        options.addOption(saveRuntimeStatistics);

        Option modCompHeuOption = Option.builder("HMODCOMP")
                .hasArg(false)
                .required(false)
                .desc("use modal components heuristics")
                .build();
        options.addOption(modCompHeuOption);

        Option onlyModalCompHeuOption = Option.builder("HMODCOMPONLY")
                .hasArg(false)
                .required(false)
                .desc("use only modal components heuristics")
                .build();
        options.addOption(onlyModalCompHeuOption);

        Option singleMetaCompHeuOption = Option.builder("HSINGLECOMP")
                .hasArg(false)
                .required(false)
                .desc("uses only one meta component heuristics")
                .build();
        options.addOption(singleMetaCompHeuOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine commandLine = null;

        try {
            commandLine = parser.parse(options, args);
        }catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("S34ELP", options);
            System.exit(1);
        }

        File file = new File(commandLine.getOptionValue("file"));

        Solver solver = new Solver();

        if (commandLine.hasOption("r")) {
            solver.setRunTimeStatisticsPath(commandLine.getOptionValue("r"));
        }

        if(commandLine.hasOption("s")) {
            solver.setDeleteTempFiles(false);
        }

        List<Heuristics> heuristics = new ArrayList<>();

        if (commandLine.hasOption("HEPONLY")) {
            heuristics.add(new OnlyModalComponentHeuristics());
        } else if (commandLine.hasOption("HMODCOMP")) {
            heuristics.add(new ModalComponentHeuristics());
        }

        if (commandLine.hasOption("HSINGLECOMP")) {
            heuristics.add(new SingleMetaCompHeuristics());
        }

        if(!heuristics.isEmpty()) {
            solver.setHeuristics(heuristics);
        }

        solver.solve(file);

    }

}
