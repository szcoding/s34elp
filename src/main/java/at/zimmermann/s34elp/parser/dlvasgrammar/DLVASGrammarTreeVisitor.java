package at.zimmermann.s34elp.parser.dlvasgrammar;

import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.component.Component;
import at.zimmermann.s34elp.entity.component.ComponentGraph;
import at.zimmermann.s34elp.entity.term.Constant;
import at.zimmermann.s34elp.entity.term.Function;
import at.zimmermann.s34elp.entity.term.Term;
import at.zimmermann.s34elp.entity.term.Variable;
import at.zimmermann.s34elp.metaprogramtransformation.MetaSymbols;

import java.util.*;

/**
 * Visitor for answerset of meta-program
 */
public class DLVASGrammarTreeVisitor extends DLVASGrammarBaseVisitor<Object> {

    private ComponentGraph componentGraph = new ComponentGraph();
    private Program program;

    private String compPredicate = MetaSymbols.META_R2C;
    private String depPredicate = MetaSymbols.META_M_COMP_DEP;

    public DLVASGrammarTreeVisitor(Program program) {
        this.program = program;
    }

    public DLVASGrammarTreeVisitor(Program program, String compPredicate, String depPredicate) {
        this.program = program;
        this.compPredicate = compPredicate;
        this.depPredicate = depPredicate;
    }

    @Override
    public Object visitStart(final DLVASGrammarParser.StartContext ctx) {
        super.visitStart(ctx);
        return componentGraph;
    }

    @Override
    public Object visitAnswersets(DLVASGrammarParser.AnswersetsContext ctx) {

        if(ctx.answerset() != null && ctx.answerset().size() > 0) {
            return visitAnswerset(ctx.answerset(0));
        }

        return null;

    }

    @Override
    public Object visitAnswerset(final DLVASGrammarParser.AnswersetContext ctx) {
        return super.visitAnswerset(ctx);
    }

    @Override
    public Object visitFacts(final DLVASGrammarParser.FactsContext ctx) {
        return super.visitFacts(ctx);
    }

    @Override
    public Object visitAtom(final DLVASGrammarParser.AtomContext ctx) {

        String pid = (String) visitPredicate_symbol(ctx.predicate_symbol());

        if (pid.startsWith(compPredicate)) {

            List<Term> terms = (List<Term>) visitTerms(ctx.terms());

            String compName = terms.get(0).toString();

            Component component = componentGraph.getComponentByName(compName);

            // if component does not exist, create a new one
            if (component == null) {
                component = new Component();
                component.setCompName(compName);
                componentGraph.addComponent(component);
            }

            // add rule to component
            if (terms.get(1) instanceof Constant) {
                Constant ruleConst = (Constant) terms.get(1);
                String ruleName = ruleConst.getValue();
                Integer ruleNo = Integer.parseInt(ruleName.replace(MetaSymbols.META_RULE, ""));
                component.addRule(ruleNo);
                component.addSubstitution(ruleNo, new HashMap<>());
            } else if (terms.get(1) instanceof Function) {
                Function ruleFunction = (Function) terms.get(1);
                String ruleName = ruleFunction.getId();
                Integer ruleNo = Integer.parseInt(ruleName.replace(MetaSymbols.META_RULE, ""));
                component.addRule(ruleNo);
                LinkedHashSet<Variable> variables = program.getRules().get(ruleNo).getVariables();
                List<Term> functionArguments = ruleFunction.getArguments();

                int i = 0;

                Map<String, String> substitution = new HashMap<>();

                for (Variable variable : variables) {
                    substitution.put(variable.getName(), functionArguments.get(i++).toString());
                }
                component.addSubstitution(ruleNo, substitution);
            }
        } else if (pid.startsWith(depPredicate)) {

            List<Term> terms = (List<Term>) visitTerms(ctx.terms());

            Component comp1 = componentGraph.getComponentByName(terms.get(0).toString());
            if (comp1 == null) {
                comp1 = new Component();
                comp1.setCompName(terms.get(0).toString());
                componentGraph.addComponent(comp1);
            }
            Component comp2 = componentGraph.getComponentByName(terms.get(1).toString());
            if (comp2 == null) {
                comp2 = new Component();
                comp2.setCompName(terms.get(1).toString());
                componentGraph.addComponent(comp2);
            }

            componentGraph.addDependency(comp1, comp2);

        }


        return super.visitAtom(ctx);
    }

    @Override
    public Object visitTerms(DLVASGrammarParser.TermsContext ctx) {
        List<Term> terms = new ArrayList<>();
        for (DLVASGrammarParser.TermContext context : ctx.term()) {
            terms.add((Term) visitTerm(context));
        }
        return terms;
    }

    @Override
    public Object visitTerm(DLVASGrammarParser.TermContext ctx) {

        if (ctx.basic_term() != null) {
            return visitBasic_term(ctx.basic_term());
        } else {
            // otherwise not supported
            return null;
        }
    }

    @Override
    public Object visitBasic_term(DLVASGrammarParser.Basic_termContext ctx) {

        if (ctx.subterm() != null) {
            return visitSubterm(ctx.subterm());
        } else if (ctx.negative_term() != null) {
            // not supported
            return null;
        } else if (ctx.functional() != null) {
            return visitFunctional(ctx.functional());
        } else if (ctx.constant() != null) {
            return visitConstant(ctx.constant());
        } else if (ctx.variable() != null) {
            return visitVariable(ctx.variable());
        } else {
            return null;
        }

    }

    @Override
    public Object visitFunctional(DLVASGrammarParser.FunctionalContext ctx) {

        Function function = new Function();
        function.setId((String) visitFunction_symbol(ctx.function_symbol()));
        if (ctx.terms() != null) {
            function.setArguments((List<Term>) visitTerms(ctx.terms()));
        }
        return function;
    }

    @Override
    public Object visitFunction_symbol(DLVASGrammarParser.Function_symbolContext ctx) {
        return ctx.ID().getText();
    }

    @Override
    public Object visitSubterm(DLVASGrammarParser.SubtermContext ctx) {
        return visitTerm(ctx.term());
    }

    @Override
    public Object visitConstant(DLVASGrammarParser.ConstantContext ctx) {
        Constant constant = new Constant();
        super.visitConstant(ctx);
        String value = null;
        if (ctx.ID() != null) {
            value = ctx.ID().getText();
        } else if (ctx.STRING() != null) {
            value = ctx.STRING().getText();
        } else {
            value = ctx.NUMBER().getText();
        }
        constant.setValue(value);
        return constant;
    }

    @Override
    public Object visitVariable(DLVASGrammarParser.VariableContext ctx) {
        Variable variable = new Variable();
        variable.setName((String) super.visitVariable(ctx));
        return variable;
    }

    @Override
    public Object visitPredicate_symbol(final DLVASGrammarParser.Predicate_symbolContext ctx) {
        return ctx.ID().getText();
    }
}
