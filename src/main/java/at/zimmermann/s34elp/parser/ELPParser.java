package at.zimmermann.s34elp.parser;

import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.exception.ParsingException;

import java.io.File;

public interface ELPParser {

    /**
     * creates an epistemic logic program from a given file
     * @param file that contains an epistemic logic program
     * @return epistemic logic program
     * @throws ParsingException
     */
    public Program parse(File file) throws ParsingException;

}
