package at.zimmermann.s34elp.parser;

import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.exception.ParsingException;
import at.zimmermann.s34elp.parser.ehexgrammar.EhexGrammarLexer;
import at.zimmermann.s34elp.parser.ehexgrammar.EhexGrammarParser;
import at.zimmermann.s34elp.parser.ehexgrammar.EhexGrammarTreeVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.File;
import java.io.IOException;

public class EhexParser implements ELPParser {

    public Program parse(File file) throws ParsingException {

        CharStream input;

        try {
            input = CharStreams.fromFileName(file.getCanonicalPath());

        } catch (IOException ex) {
            throw new ParsingException(ex.getMessage());
        }

        EhexGrammarLexer lexer = new EhexGrammarLexer(input);
        CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
        EhexGrammarParser parser = new EhexGrammarParser(commonTokenStream);
        ParseTree parseTree = parser.start();
        Program program = (Program) new EhexGrammarTreeVisitor().visit(parseTree);

        return program;

    }

}
