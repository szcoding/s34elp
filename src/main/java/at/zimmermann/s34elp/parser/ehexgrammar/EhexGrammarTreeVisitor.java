package at.zimmermann.s34elp.parser.ehexgrammar;

import at.zimmermann.s34elp.entity.Body;
import at.zimmermann.s34elp.entity.Head;
import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.Rule;
import at.zimmermann.s34elp.entity.atom.Atom;
import at.zimmermann.s34elp.entity.atom.BuiltinAtom;
import at.zimmermann.s34elp.entity.atom.Literal;
import at.zimmermann.s34elp.entity.term.Constant;
import at.zimmermann.s34elp.entity.term.Function;
import at.zimmermann.s34elp.entity.term.Term;
import at.zimmermann.s34elp.entity.term.Variable;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Parser for ehex-program
 */
public class EhexGrammarTreeVisitor extends EhexGrammarBaseVisitor<Object> {

    private Program program;
    private Integer ruleNumber = 1;

    @Override
    public Object visitStart(EhexGrammarParser.StartContext ctx) {

        program = new Program();
        super.visitStart(ctx);
        return program;

    }

    @Override
    public Object visitProgram(EhexGrammarParser.ProgramContext ctx) {
        return super.visitProgram(ctx);
    }

    @Override
    public Object visitStatements(EhexGrammarParser.StatementsContext ctx) {
        return super.visitStatements(ctx);
    }

    @Override
    public Object visitCons(EhexGrammarParser.ConsContext ctx) {
        Rule rule = (Rule) super.visitCons(ctx);
        program.addRule(ruleNumber++, rule);
        return null;
    }

    @Override
    public Object visitR_rul(EhexGrammarParser.R_rulContext ctx) {
        Rule rule = (Rule) super.visitR_rul(ctx);
        program.addRule(ruleNumber++, rule);
        return null;
    }

    @Override
    public Object visitWeak_con(EhexGrammarParser.Weak_conContext ctx) {
        return super.visitWeak_con(ctx);
    }

    @Override
    public Object visitOpt(EhexGrammarParser.OptContext ctx) {
        return super.visitOpt(ctx);
    }

    @Override
    public Object visitConstraint(EhexGrammarParser.ConstraintContext ctx) {
        Rule rule = new Rule();
        Body body = (Body) visitBody(ctx.body());
        rule.setBody(body);
        return rule;
    }

    @Override
    public Object visitR_rule(EhexGrammarParser.R_ruleContext ctx) {
        Rule rule = new Rule();
        Head head = (Head) visitHead(ctx.head());
        Body body = null;
        if (ctx.body() != null) {
            body = (Body) visitBody(ctx.body());
        }
        rule.setHead(head);
        rule.setBody(body);
        return rule;
    }

    @Override
    public Object visitHead(EhexGrammarParser.HeadContext ctx) {
        Head head = new Head();
        List<Literal> headLiterals = null;
        if (ctx.disjunction() != null) {
            headLiterals = (List<Literal>) visitDisjunction(ctx.disjunction());
        }
        head.setHead(headLiterals);
        return head;
    }

    @Override
    public Object visitTerminal(TerminalNode node) {
        super.visitTerminal(node);
        return node.getText().toString();
    }

    @Override
    public Object visitBody(EhexGrammarParser.BodyContext ctx) {

        Body body = new Body();
        List<Literal> bodyLiterals = new ArrayList<>();
        for (EhexGrammarParser.Extended_literalContext context : ctx.extended_literal()) {
            Literal literal = (Literal) visitExtended_literal(context);
            bodyLiterals.add(literal);
        }
        body.setBodyList(bodyLiterals);
        return body;
    }

    @Override
    public Object visitDisjunction(EhexGrammarParser.DisjunctionContext ctx) {
        List<Literal> headLiterals = new ArrayList<>();

        for (EhexGrammarParser.Classical_literalContext context : ctx.classical_literal()) {
            Literal literal = (Literal) visitClassical_literal(context);
            headLiterals.add(literal);
        }

        return headLiterals;
    }

    @Override
    public Object visitClassical_literal(EhexGrammarParser.Classical_literalContext ctx) {

        Literal literal = new Literal();

        if (ctx.strong_negated_atom() != null) {
            literal = (Literal) visit(ctx.strong_negated_atom());
        } else if (ctx.atom() != null) {
            literal.setAtom((Atom) visitAtom(ctx.atom()));
        }

        return literal;
    }

    @Override
    public Object visitAtom(EhexGrammarParser.AtomContext ctx) {
        Atom atom = new Atom();

        atom.setId((String) visit(ctx.predicate_symbol()));

        if (ctx.terms() != null) {
            atom.setArguments((List<Term>) visitTerms(ctx.terms()));
        }

        return atom;
    }

    @Override
    public Object visitStrong_negated(EhexGrammarParser.Strong_negatedContext ctx) {
        Literal literal = new Literal();
        literal.setNegated(true);
        literal.setAtom((Atom) visitAtom(ctx.atom_));
        return literal;
    }

    @Override
    public Object visitId(EhexGrammarParser.IdContext ctx) {
        return ctx.id.getText();
    }

    @Override
    public Object visitExtended_literals(EhexGrammarParser.Extended_literalsContext ctx) {
        List<Literal> literals = new ArrayList<>();

        for (EhexGrammarParser.Extended_literalContext context : ctx.extended_literal()) {
            Literal literal = (Literal) visitExtended_literal(context);
            literals.add(literal);
        }

        return literals;
    }

    @Override
    public Object visitExtended_literal(EhexGrammarParser.Extended_literalContext ctx) {

        Literal literal = null;

        if (ctx.default_negated_literal() != null) {
            literal = (Literal) visit(ctx.default_negated_literal());
        } else if (ctx.modal_literal() != null) {
            literal = (Literal) visitModal_literal(ctx.modal_literal());
        } else if (ctx.classical_literal() != null) {
            literal = (Literal) visitClassical_literal(ctx.classical_literal());
        } else if (ctx.builtin_atom() != null) {
            // not supported
            literal = new Literal();
            literal.setAtom((BuiltinAtom) visitBuiltin_atom(ctx.builtin_atom()));
        }

        return literal;

    }

    @Override
    public Object visitBuiltin_atom(final EhexGrammarParser.Builtin_atomContext ctx) {
        return super.visitBuiltin_atom(ctx);
    }

    @Override
    public Object visitBinary_relation(final EhexGrammarParser.Binary_relationContext ctx) {
        BuiltinAtom builtinAtom = new BuiltinAtom();

        builtinAtom.setId(ctx.relational_op().getText());
        if (ctx.term() != null && ctx.term().size() == 2) {
            EhexGrammarParser.TermContext termLeft = ctx.term(0);
            EhexGrammarParser.TermContext termRight = ctx.term(1);
            List<Term> arguments = new ArrayList<>();
            arguments.add((Term) visitTerm(termLeft));
            arguments.add((Term) visitTerm(termRight));
            builtinAtom.setArguments(arguments);
        }

        return builtinAtom;
    }

    @Override
    public Object visitDefault_negated_lit(EhexGrammarParser.Default_negated_litContext ctx) {

        Literal l = new Literal();

        if (ctx.modal_literal() != null) {
            l = (Literal) visitModal_literal(ctx.modal_literal());
        } else if (ctx.classical_literal() != null) {
            l = (Literal) visitClassical_literal(ctx.classical_literal());
        } else if (ctx.builtin_atom() != null) {
            l = (Literal) visitBuiltin_atom(ctx.builtin_atom());
        }

        l.setDefaultNegated(true);

        return l;
    }

    @Override
    public Object visitModal_literal(EhexGrammarParser.Modal_literalContext ctx) {
        Literal literal = (Literal) super.visitModal_literal(ctx);
        return literal;
    }

    @Override
    public Object visitK_mod(EhexGrammarParser.K_modContext ctx) {
        Literal modalLiteral = new Literal();
        modalLiteral = (Literal) visitClassical_literal(ctx.literal_);
        modalLiteral.setModal(Literal.MODAL.K);
        return modalLiteral;
    }

    @Override
    public Object visitM_mod(EhexGrammarParser.M_modContext ctx) {
        Literal modalLiteral = new Literal();
        modalLiteral = (Literal) visitClassical_literal(ctx.literal_);
        modalLiteral.setModal(Literal.MODAL.M);
        return modalLiteral;
    }

    @Override
    public Object visitTerms(EhexGrammarParser.TermsContext ctx) {
        List<Term> terms = new ArrayList<>();
        for (EhexGrammarParser.TermContext context : ctx.term()) {
            terms.add((Term) visitTerm(context));
        }
        return terms;
    }

    @Override
    public Object visitTerm(EhexGrammarParser.TermContext ctx) {

        if (ctx.basic_term() != null) {
            return visitBasic_term(ctx.basic_term());
        } else {
            // otherwise not supported
            return null;
        }
    }

    @Override
    public Object visitBasic_term(EhexGrammarParser.Basic_termContext ctx) {

        if (ctx.subterm() != null) {
            return visitSubterm(ctx.subterm());
        } else if (ctx.negative_term() != null) {
            // not supported
            return null;
        } else if (ctx.functional() != null) {
            return visitFunctional(ctx.functional());
        } else if (ctx.constant() != null) {
            return visitConstant(ctx.constant());
        } else if (ctx.variable() != null) {
            return visitVariable(ctx.variable());
        } else {
            return null;
        }

    }

    @Override
    public Object visitFunctional(EhexGrammarParser.FunctionalContext ctx) {

        Function function = new Function();
        function.setId((String) visitFunction_symbol(ctx.function_symbol()));
        if (ctx.terms() != null) {
            function.setArguments((List<Term>) visitTerms(ctx.terms()));
        }
        return function;
    }

    @Override
    public Object visitFunction_symbol(EhexGrammarParser.Function_symbolContext ctx) {
        return ctx.ID().getText();
    }

    @Override
    public Object visitSubterm(EhexGrammarParser.SubtermContext ctx) {
        return visitTerm(ctx.term());
    }

    @Override
    public Object visitConstant(EhexGrammarParser.ConstantContext ctx) {
        Constant constant = new Constant();
        constant.setValue((String) super.visitConstant(ctx));
        return constant;
    }

    @Override
    public Object visitVariable(EhexGrammarParser.VariableContext ctx) {
        Variable variable = new Variable();
        variable.setName((String) super.visitVariable(ctx));
        return variable;
    }


}
