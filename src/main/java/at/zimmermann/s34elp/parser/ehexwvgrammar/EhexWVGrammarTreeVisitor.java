package at.zimmermann.s34elp.parser.ehexwvgrammar;

import at.zimmermann.s34elp.entity.atom.Atom;
import at.zimmermann.s34elp.entity.atom.Literal;
import at.zimmermann.s34elp.entity.solution.AnswerSet;
import at.zimmermann.s34elp.entity.solution.WorldView;
import at.zimmermann.s34elp.entity.term.Constant;
import at.zimmermann.s34elp.entity.term.Function;
import at.zimmermann.s34elp.entity.term.Term;
import at.zimmermann.s34elp.entity.term.Variable;

import java.util.ArrayList;
import java.util.List;

/**
 * Visitor for answerset of component-program
 */
public class EhexWVGrammarTreeVisitor extends EhexWVGrammarBaseVisitor<Object> {

    private List<WorldView> worldViews = new ArrayList<>();
    private WorldView currentWorldView = null;
    private boolean onlyEpistemicGuesses = false;

    public void setOnlyEpistemicGuesses(boolean onlyEpistemicGuesses) {
        this.onlyEpistemicGuesses = onlyEpistemicGuesses;
    }

    @Override
    public Object visitStart(EhexWVGrammarParser.StartContext ctx) {
        super.visitStart(ctx);
        return worldViews;
    }

    @Override
    public Object visitWorld_view(EhexWVGrammarParser.World_viewContext ctx) {
        currentWorldView = new WorldView();
        worldViews.add(currentWorldView);
        return super.visitWorld_view(ctx);
    }

    @Override
    public Object visitAnswersets(EhexWVGrammarParser.AnswersetsContext ctx) {

        if(!onlyEpistemicGuesses) {
            for (EhexWVGrammarParser.AnswersetContext context : ctx.answerset()) {
                AnswerSet answerSet = new AnswerSet();
                List<Literal> literals = (List<Literal>) visitAnswerset(context);
                answerSet.setAnswerSets(literals);
                currentWorldView.addAnswerSet(answerSet);
            }
        }
        return null;
    }

    @Override
    public Object visitEpistemic_guess(EhexWVGrammarParser.Epistemic_guessContext ctx) {
        List<Literal> epistemicGuesses =  (List<Literal>)visitFacts(ctx.facts());
        currentWorldView.addEpistemicGuess(epistemicGuesses);
        return null;
    }

    @Override
    public Object visitAnswerset(EhexWVGrammarParser.AnswersetContext ctx) {

        if(ctx != null && ctx.facts() != null) {
            return visitFacts(ctx.facts());
        }
        return null;
    }

    @Override
    public Object visitFacts(EhexWVGrammarParser.FactsContext ctx) {

        List<Literal> literals = new ArrayList<>();

        if(ctx != null) {
            for (EhexWVGrammarParser.Extended_literalContext context : ctx.extended_literal()) {
                Literal l = (Literal) visitExtended_literal(context);
                literals.add(l);
            }
        }

        return literals;
    }

    @Override
    public Object visitExtended_literal(EhexWVGrammarParser.Extended_literalContext ctx) {
        return super.visitExtended_literal(ctx);
    }

    @Override
    public Object visitDefault_negated_literal(EhexWVGrammarParser.Default_negated_literalContext ctx) {
        Literal literal = (Literal) super.visitDefault_negated_literal(ctx);
        literal.setDefaultNegated(true);
        return literal;
    }

    @Override
    public Object visitK_mod(EhexWVGrammarParser.K_modContext ctx) {
        Literal literal = (Literal) super.visitK_mod(ctx);
        literal.setModal(Literal.MODAL.K);
        return literal;
    }

    @Override
    public Object visitM_mod(EhexWVGrammarParser.M_modContext ctx) {
        Literal literal = (Literal) super.visitM_mod(ctx);
        literal.setModal(Literal.MODAL.M);
        return literal;
    }

    @Override
    public Object visitClassical_literal(EhexWVGrammarParser.Classical_literalContext ctx) {
        Literal literal;
        if (ctx.atom() != null) {
            literal = new Literal();
            literal.setAtom((Atom) visitAtom(ctx.atom()));
        } else {
            literal = (Literal) visit(ctx.strong_negated_atom());
        }
        return literal;
    }

    @Override
    public Object visitStrong_negated_atom(EhexWVGrammarParser.Strong_negated_atomContext ctx) {
        Literal literal = new Literal();
        literal.setAtom((Atom) super.visitStrong_negated_atom(ctx));
        literal.setNegated(true);
        return literal;
    }

    @Override
    public Object visitAtom(EhexWVGrammarParser.AtomContext ctx) {
        Atom atom = new Atom();
        atom.setId(ctx.predicate_symbol().getText());
        if (ctx.terms() != null) {
            atom.setArguments((List<Term>) visitTerms(ctx.terms()));
        }
        return atom;
    }

    @Override
    public Object visitTerms(EhexWVGrammarParser.TermsContext ctx) {
        List<Term> terms = new ArrayList<>();
        for (EhexWVGrammarParser.TermContext context : ctx.term()) {
            terms.add((Term) visitTerm(context));
        }
        return terms;
    }

    @Override
    public Object visitTerm(EhexWVGrammarParser.TermContext ctx) {

        if (ctx.basic_term() != null) {
            return visitBasic_term(ctx.basic_term());
        } else {
            // otherwise not supported
            return null;
        }
    }

    @Override
    public Object visitBasic_term(EhexWVGrammarParser.Basic_termContext ctx) {

        if (ctx.subterm() != null) {
            return visitSubterm(ctx.subterm());
        } else if (ctx.negative_term() != null) {
            // not supported
            return null;
        } else if (ctx.functional() != null) {
            return visitFunctional(ctx.functional());
        } else if (ctx.constant() != null) {
            return visitConstant(ctx.constant());
        } else if (ctx.variable() != null) {
            return visitVariable(ctx.variable());
        } else {
            return null;
        }

    }

    @Override
    public Object visitFunctional(EhexWVGrammarParser.FunctionalContext ctx) {

        Function function = new Function();
        function.setId(ctx.function_symbol().getText());
        if (ctx.terms() != null) {
            function.setArguments((List<Term>) visitTerms(ctx.terms()));
        }
        return function;
    }

    @Override
    public Object visitSubterm(EhexWVGrammarParser.SubtermContext ctx) {
        return visitTerm(ctx.term());
    }

    @Override
    public Object visitConstant(EhexWVGrammarParser.ConstantContext ctx) {
        Constant constant = new Constant();
        constant.setValue(ctx.getText());
        return constant;
    }

    @Override
    public Object visitVariable(EhexWVGrammarParser.VariableContext ctx) {
        Variable variable = new Variable();
        variable.setName((String) super.visitVariable(ctx));
        return variable;
    }
}
