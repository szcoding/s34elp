package at.zimmermann.s34elp;

import at.zimmermann.s34elp.componentcreation.ComponentGraphGenerator;
import at.zimmermann.s34elp.componentevaluation.ComponentEvaluation;
import at.zimmermann.s34elp.entity.Program;
import at.zimmermann.s34elp.entity.component.ComponentGraph;
import at.zimmermann.s34elp.entity.solution.WorldView;
import at.zimmermann.s34elp.exception.ComponentEvaluationException;
import at.zimmermann.s34elp.exception.NoValidProgramException;
import at.zimmermann.s34elp.exception.ParsingException;
import at.zimmermann.s34elp.metaprogramtransformation.MetaProgramTransformation;
import at.zimmermann.s34elp.metaprogramtransformation.heuristics.BasicHeuristics;
import at.zimmermann.s34elp.metaprogramtransformation.heuristics.Heuristics;
import at.zimmermann.s34elp.parser.ELPParser;
import at.zimmermann.s34elp.parser.EhexParser;
import at.zimmermann.s34elp.solver.ASPSolver;
import at.zimmermann.s34elp.solver.impl.DLVSolver;
import at.zimmermann.s34elp.util.RunTimeStatistics;
import at.zimmermann.s34elp.util.RunTimeStatisticsEntry;
import at.zimmermann.s34elp.util.TempFileCreator;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Solver {

    final static Logger LOGGER = Logger.getLogger(Solver.class);

    private final ELPParser elpParser;
    private final MetaProgramTransformation metaProgramTransformation;
    private final ComponentGraphGenerator componentGraphGenerator;
    private final ASPSolver aspSolver;
    private final ComponentEvaluation evaluationComponent;
    private final BasicHeuristics basicHeuristics;
    private List<Heuristics> heuristics = null;
    private final RunTimeStatistics runTimeStatistics = new RunTimeStatistics();
    private String runTimeStatisticsPath = null;

    private long startedAction;

    private boolean deleteTempFiles = true;

    public Solver() {

        elpParser = new EhexParser();
        metaProgramTransformation = new MetaProgramTransformation();
        componentGraphGenerator = new ComponentGraphGenerator();
        aspSolver = new DLVSolver(1);
        evaluationComponent = new ComponentEvaluation(runTimeStatistics);
        basicHeuristics = new BasicHeuristics();
    }

    public void solve(File file) throws IOException {

        try {

            startedAction = System.currentTimeMillis();
            Program inputProgram = elpParser.parse(file);
            runTimeStatistics.addEntry(new RunTimeStatisticsEntry("Parsing", System.currentTimeMillis() - startedAction));
            LOGGER.debug("finished parsing input program..");

            startedAction = System.currentTimeMillis();
            StringBuilder metaProgram = metaProgramTransformation.generateMetaProgram(inputProgram);
            LOGGER.debug("finished creating meta program..");

            if(heuristics != null && !heuristics.isEmpty()) {
                metaProgram.append(basicHeuristics.apply(inputProgram));
                for (Heuristics heuristic : heuristics) {
                    metaProgram.append(heuristic.apply(inputProgram));
                }
                LOGGER.debug("adding heuristics..");
            }

            File metaProgramFile = TempFileCreator.createTempFile("meta", "dl", metaProgram.toString());
            LOGGER.debug("created meta program file..");

            setPredicates();
            LOGGER.debug("setting predicate filter for asp solver and component graph generator..");
            runTimeStatistics.addEntry(new RunTimeStatisticsEntry("Metaprogram Transformation", System.currentTimeMillis() - startedAction));

            startedAction = System.currentTimeMillis();
            String answerSetsResult = aspSolver.solve(metaProgramFile);
            LOGGER.debug("solved meta program..");

            if(deleteTempFiles) {
                metaProgramFile.delete();
                LOGGER.debug("deleted meta-program file..");
            }

            runTimeStatistics.addEntry(new RunTimeStatisticsEntry("ASP Solver", System.currentTimeMillis() - startedAction));

            startedAction = System.currentTimeMillis();
            ComponentGraph componentGraph = componentGraphGenerator.generate(inputProgram, answerSetsResult);
            LOGGER.debug("created component graph..");

            runTimeStatistics.addEntry(new RunTimeStatisticsEntry("Component-Graph Generator", System.currentTimeMillis() - startedAction));
            runTimeStatistics.addEntry(new RunTimeStatisticsEntry("No of components", (long) componentGraph.getOrderedComponentListForEvaluation().size()));

            startedAction = System.currentTimeMillis();
            List<WorldView> resultList = evaluationComponent.evaluateComponents(inputProgram, componentGraph);
            LOGGER.debug("Finished solving components..");
            runTimeStatistics.addEntry(new RunTimeStatisticsEntry("Component Evaluation", System.currentTimeMillis() - startedAction));

            if (resultList != null) {
                for (WorldView wv : resultList) {
                    System.out.println(wv.toString());
                }
            } else {
                System.out.println("No world view");
            }

            if (runTimeStatisticsPath != null) {
                writeRuntimeStatisticsToFile();
            }

        } catch (ParsingException ex) {
            System.out.println("error while parsing: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("error creating meta program: " + ex.getMessage());
        } catch (NoValidProgramException ex) {
            System.out.println("not a valid program: " + ex.getMessage());
        } catch (ComponentEvaluationException ex) {
            System.out.println("error while evaluating the components: " + ex.getMessage());
        }


    }

    private void setPredicates() {
        if(heuristics == null || heuristics.isEmpty()) {
            aspSolver.setPredicateFilter(metaProgramTransformation.getCompPredicates());
            componentGraphGenerator.setCompPredicate(metaProgramTransformation.getCompPredicate());
            componentGraphGenerator.setDepPredicate(metaProgramTransformation.getDepPredicate());
        } else {
            aspSolver.setPredicateFilter(basicHeuristics.getCompPredicates());
            componentGraphGenerator.setCompPredicate(basicHeuristics.getCompPredicate());
            componentGraphGenerator.setDepPredicate(basicHeuristics.getDepPredicate());
        }
    }

    public boolean isDeleteTempFiles() {
        return deleteTempFiles;
    }

    public void setDeleteTempFiles(boolean deleteTempFiles) {
        this.deleteTempFiles = deleteTempFiles;
        evaluationComponent.deleteTempFiles(deleteTempFiles);
    }

    public List<Heuristics> getHeuristics() {
        return heuristics;
    }


    public void setHeuristics(List<Heuristics> heuristics) {
        this.heuristics = heuristics;
    }

    public void setRunTimeStatisticsPath(String runTimeStatisticsPath) {
        this.runTimeStatisticsPath = runTimeStatisticsPath;
    }

    private void writeRuntimeStatisticsToFile() throws IOException {
        File f = new File(runTimeStatisticsPath);

        FileWriter fileWriter = null;

        if (!f.exists()) {
            f.createNewFile();
            fileWriter = new FileWriter(f);
            fileWriter.append(runTimeStatistics.toString());
        } else {
            fileWriter = new FileWriter(f, true);
            fileWriter.append("\n" + runTimeStatistics.dataToString());
        }

        fileWriter.flush();
        fileWriter.close();
    }


}
